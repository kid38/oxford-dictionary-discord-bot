const Discord = require('discord.js');
const client = new Discord.Client();
const fs = require('fs');
const request = require('request-promise-native');

var settings = JSON.parse(fs.readFileSync('settings.json', 'utf8'));
var bot_token = settings.bot_token;

var app_id = settings.oxford_api_id;
var app_key = settings.oxford_api_key;

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', async (msg) => {
  if (msg.content.startsWith("!meaning")) {
    try
    {
        var word = /!meaning (.*)/.exec(msg.content)[1];
        var inflection_result = await getWordInflection(word);
        var inflections = [];
        inflection_result.results[0].lexicalEntries.map((element) => {
            if(inflections.find(x => x == element.inflectionOf[0].text) == undefined)
            {
                inflections.push(element.inflectionOf[0].text);
            }
        });
        //put initial word first
        inflections.sort((a, b) => {
            if(a == word)
            {
                return -1;
            }
            if(b == word)
            {
                return 1;
            }
        });
        var result_text = "";
        await Promise.all(inflections.map(async (element) => {
            var word_result = await getWordMeaning(element);
            result_text += `\n**${word_result.results[0].word}**\n`;
            word_result.results[0].lexicalEntries.forEach((lexical_entry) => {
                if(lexical_entry.lexicalCategory != "Other")
                {
                    result_text += `${lexical_entry.lexicalCategory}\n`;
                    result_text += `[${lexical_entry.pronunciations[0].phoneticSpelling}]\n`;
                    lexical_entry.entries.forEach((meaning) => {
                        if(meaning.etymologies != undefined)
                        {
                            result_text += `  Etymology: ${meaning.etymologies[0]}\n`;                    
                        }
                        meaning.senses.forEach((sense) => {
                            if(sense.definitions != undefined)
                            {
                                result_text += `    • ${sense.definitions[0]}\n`;                        
                            }
                            if(sense.examples != undefined)
                            {
                                result_text += ` *${sense.examples[0].text}*\n`;
                            }
                        });
                    });     
                    result_text += `\n`;
                }
            });
        }))
    
        var embed_message = {
            embed: {
                color: 0x00AE86,
                author: {
                    name: `Meaning of "${word}"`
                },
                footer: {
                    text: "From Oxford Dictionaries"
                },
                description: result_text
            }
        };
        msg.channel.send(embed_message);
    }
    catch(error)
    {
        console.error(error);
        msg.channel.send("The word not found.");
    }
  }
});

async function getWordMeaning(word)
{
    var result = await readAPI(`https://od-api.oxforddictionaries.com/api/v1/entries/en/${word}`);
    return JSON.parse(result);
}

async function getWordInflection(word)
{
    var result = await readAPI(`https://od-api.oxforddictionaries.com/api/v1/inflections/en/${word}`);
    return JSON.parse(result);
}

function readAPI(url)
{
    return new Promise((resolve, reject) => {
        var options = {
            headers: {
              'app_id': app_id,
              'app_key': app_key
            }
        };   
        request.get(url, options).then((data) => resolve(data)).catch((error) => reject(error));
    })
}

client.login(bot_token);